abstract class Employee{
    String Eponimia;
    int PayType;

    public Employee(String Eponimia, int PayType) {
        this.Eponimia = Eponimia;
        this.PayType = PayType;
    }

    public String getEponimia() {
        return Eponimia;
    }


    @Override
    public String toString() {
        return "Employee{" + "Eponimia=" + Eponimia + ", PayType=" + PayType + '}';
    }
    
    abstract public String getPayType();
    
    public void Katharos_Misthotou(){}
    public void Katharos_Oromisthiou(){}
    public void Katharos_Symvasiouhou(){}
    
}

// INTER

interface iMisthos {
    public void Katharos_Misthotou();
}

interface iOromisthios {
    public void Katharos_Oromisthiou();

}

interface   iSymvasiouhos{
    public void Katharos_Symvasiouhou();
}

//MISTHOS

class Misthotos extends Employee implements iMisthos{

    double akatharistos;
    double kratisis;
    double bonus_Paidion;

    public Misthotos( String Eponimia) {
        super(Eponimia, 0);
       
    }
    
    

    public Misthotos( String Eponimia ,double akatharistos, double kratisis, double bonus_Paidion) {
        super(Eponimia, 0);
        this.akatharistos = akatharistos;
        this.kratisis = kratisis;
        this.bonus_Paidion = bonus_Paidion;
    }
    

    @Override
    public String getPayType() {
        return "PayType="+PayType;
    }

    @Override
    public void Katharos_Misthotou() {
       
       double k= akatharistos-kratisis+bonus_Paidion;
        System.out.println(toString() +" | Katharos_Misthotou = "+ k);
    }  

    @Override
    public String toString() {
        return String.format("%15s%15s" ,"Eponimia = "+Eponimia  ," | "+getPayType() );

    }
    
}


//OROS

class Oromisthios extends Employee implements iOromisthios{

    double ores_ergasias;
    double oromisthio;
    
    public Oromisthios(String Eponimia) {
        super(Eponimia, 1);
    }

    public Oromisthios(String Eponimia,double ores_ergasias, double oromisthio) {
        super(Eponimia, 1);
        this.ores_ergasias = ores_ergasias;
        this.oromisthio = oromisthio;
    }
    

    @Override
    public String getPayType() {
        return "PayType="+PayType;
    }

    @Override
    public void Katharos_Oromisthiou() {
        double k= (ores_ergasias*oromisthio-(ores_ergasias*oromisthio)*0.10);
        System.out.println(toString()+" | Katharos_Oromisthiou = "+ k );
        
    }

    @Override
    public String toString() {
        return String.format("%15s%15s" ,"Eponimia = "+Eponimia  ," | "+getPayType() );

    }
    
    
}


//SYMVA

class Symvasiouho extends Employee implements iSymvasiouhos{

    double akatharistos;
    double kratisis;
    int DiarkiaSymvasis;
    
    public Symvasiouho(String Eponimia) {
        super(Eponimia, 2);
    }

    public Symvasiouho(String Eponimia,double akatharistos, double kratisis, int DiarkiaSymvasis) {
        super(Eponimia, 2);
        this.akatharistos = akatharistos;
        this.kratisis = kratisis;
        this.DiarkiaSymvasis = DiarkiaSymvasis;
    }
    
    

    @Override
    public String getPayType() {
        return "PayType = "+PayType;
    }

    @Override
    public void Katharos_Symvasiouhou() {
     
        double k = (akatharistos-kratisis-(akatharistos-kratisis)*DiarkiaSymvasis/100);
        System.out.println(toString()+" | Katharos_Oromisthiou = "+ k );
    }

    @Override
    public String toString() {
        return String.format("%15s%15s" ,"Eponimia = "+Eponimia  ," | "+getPayType() );
       
    }
    
    

    
}

class YpologismosMisthoyYpallilou{

    public static void main(String args[]){
        
        int i = 0;
        
        Employee[] n = new Employee[3];
        
        n[i++]= new Misthotos("Test1",1,2.1,2.1);
        n[i++] = new Oromisthios("Test2",10,25);
        n[i++] = new Symvasiouho("Test3",1.000,200,6);
        
        for(i=0; i< n.length ; i++){
            
            
            if(n[i] instanceof Misthotos) { n[i].Katharos_Misthotou();}
            else if(n[i] instanceof Oromisthios) { n[i].Katharos_Oromisthiou();}
            else if(n[i] instanceof Symvasiouho) { n[i].Katharos_Symvasiouhou();}
               
               
           
        }
        
    }
}
