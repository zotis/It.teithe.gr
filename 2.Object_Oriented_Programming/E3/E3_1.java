//===Exceptions
class PayTypeException extends Exception {

    public PayTypeException(String message) {
        super(message);
    }

}

class BonusException extends Exception {

    public BonusException(String message) {
        super(message);
    }

}

class PayTypeExceptionMisthotos extends PayTypeException {

    public PayTypeExceptionMisthotos(String message) {
        super(message);
    }

}

class PayTypeExceptionOromisthios extends PayTypeException {

    public PayTypeExceptionOromisthios(String message) {
        super(message);
    }

}

class PayTypeExceptionMeSymvasi extends PayTypeException {

    public PayTypeExceptionMeSymvasi(String message) {
        super(message);
    }
}

//===Abstracts

abstract class Employee {

    String Eponimia;
    int PayType;

    //CONSTACTOR
    public Employee(String Eponimia, int PayType) {
        this.Eponimia = Eponimia;
        this.PayType = PayType;
    }

    //METHODS
    public String getEponimia() {
        return Eponimia;
    }

    @Override
    public String toString() {
        return "Employee{" + "Eponimia=" + Eponimia + ", PayType=" + PayType + '}';
    }

    abstract public String getPayType() throws PayTypeException;

    public void Katharos_Misthotou() throws BonusException {
    }

    public void Katharos_Oromisthiou() {
    }

    public void Katharos_Symvasiouhou() {
    }

}

//====INTERFACE
interface iMisthos {

    public void Katharos_Misthotou() throws BonusException;
}

interface iOromisthios {

    public void Katharos_Oromisthiou();

}

interface iSymvasiouhos {

    public void Katharos_Symvasiouhou();
}

//===MISTHOS
final class Misthotos extends Employee implements iMisthos {

    double akatharistos;
    double kratisis;
    double bonus_Paidion;
    boolean bonus;

    //CONSTACTOR
    public Misthotos(String Eponimia) {
        super(Eponimia, 0);

    }

    public Misthotos(String Eponimia, double akatharistos, double kratisis, double bonus_Paidion, int PayType) {
        super(Eponimia, PayType);
        this.akatharistos = akatharistos;
        this.kratisis = kratisis;
        this.bonus_Paidion = bonus_Paidion;
        bonus = false;

        if (bonus_Paidion > 0) {
            bonus = true;
        }
    }

    //METHODS
    @Override
    public String getPayType() throws PayTypeExceptionMisthotos {

        String message = "";

        if ((PayType < 0) || PayType > 2) {
            throw new PayTypeExceptionMisthotos("To PayType eine (pano h kato) apo to orio.");
        }

        switch (PayType) {
            case 0:
                message = "misthotos";
                break;
            case 1:
                message = "oromisthios";
                break;
            case 2:
                message = "symvasiouhos";
                break;

            default:
                message = "error";
                break;
        }
        return message;

    }

    @Override
    public void Katharos_Misthotou() throws BonusException {
        double k = 0;

        if (bonus) {
            k = akatharistos - kratisis + bonus_Paidion;
            System.out.println(toString() + " | Katharos_Misthotou = " + k);

        } else if (bonus == false) {
            k = akatharistos - kratisis;
            System.out.println(toString() + " | Katharos_Misthotou = " + k);

            throw new BonusException("To pedio ( bonus_Paidion ) den simperilamvanete.");
        }

    }

    @Override
    public String toString() {
        try {
            return String.format("%15s%15s", "Eponimia = " + Eponimia, " | " + getPayType());
        } catch (PayTypeException ex) {
            System.out.println(ex);
        }
        return String.format("%15s%10s \n", "Eponimia = " + Eponimia, " | error PayType ");

    }

}

//===Oromisthios
class Oromisthios extends Employee implements iOromisthios {

    double ores_ergasias;
    double oromisthio;

    //CONSTACTOR
    public Oromisthios(String Eponimia) {
        super(Eponimia, 1);
    }

    public Oromisthios(String Eponimia, double oromisthio, double ores_ergasias,  int PayType) {
        super(Eponimia, PayType);
        this.ores_ergasias = ores_ergasias;
        this.oromisthio = oromisthio;
    }

    //METHODS
    @Override
    public String getPayType() throws PayTypeExceptionOromisthios {
        String message = "";

        if ((PayType < 0) || PayType > 2) {
            throw new PayTypeExceptionOromisthios("To PayType eine (pano h kato) apo to orio.");
        }

        switch (PayType) {
            case 0:
                message = "misthotos";
                break;
            case 1:
                message = "oromisthios";
                break;
            case 2:
                message = "symvasiouhos";
                break;

            default:
                message = "error";
                break;
        }
        return message;
    }

    @Override
    public void Katharos_Oromisthiou() {
        double k = (ores_ergasias * oromisthio - (ores_ergasias * oromisthio) * 0.10);
        System.out.println(toString() + " | Katharos_Oromisthiou = " + k);

    }

    @Override
    public String toString() {
        try {
            return String.format("%15s%15s", "Eponimia = " + Eponimia, " | " + getPayType());
        } catch (PayTypeException ex) {
            System.out.println(ex);;
        }
        return String.format("%15s%10s ", "Eponimia = " + Eponimia, " | error PayType ");

    }

}

//==Symvasiouhos
class Symvasiouho extends Employee implements iSymvasiouhos {

    double akatharistos;
    double kratisis;
    int DiarkiaSymvasis;

    //CONSTACTOR
    public Symvasiouho(String Eponimia) {
        super(Eponimia, 2);
    }

    public Symvasiouho(String Eponimia, double akatharistos, double kratisis, int DiarkiaSymvasis, int PayType) {
        super(Eponimia, PayType);
        this.akatharistos = akatharistos;
        this.kratisis = kratisis;
        this.DiarkiaSymvasis = DiarkiaSymvasis;
    }

    //METHODS
    @Override
    public String getPayType() throws PayTypeExceptionMeSymvasi {
        String message = "";

        if ((PayType < 0) || PayType > 2) {
            throw new PayTypeExceptionMeSymvasi("To PayType eine (pano h kato) apo to orio.");
        }

        switch (PayType) {
            case 0:
                message = "misthotos";
                break;
            case 1:
                message = "oromisthios";
                break;
            case 2:
                message = "symvasiouhos";
                break;

            default:
                message = "error";
                break;
        }
        return message;
    }

    @Override
    public void Katharos_Symvasiouhou() {

        double k = (akatharistos - kratisis - (akatharistos - kratisis) * DiarkiaSymvasis / 100);
        System.out.println(toString() + " | Katharos_Oromisthiou = " + k);
    }

    @Override
    public String toString() {
        try {
            return String.format("%15s%15s", "Eponimia = " + Eponimia, " | " + getPayType());
        } catch (PayTypeException ex) {
            System.out.println(ex);
        }
        return String.format("%15s%10s", "Eponimia = " + Eponimia, " | error PayType ");
    }

}

class E3_1 {

    public static void main(String args[]) {

        int i = 0;

        Employee[] n = new Employee[3];

        n[i++] = new Misthotos("Petros M", 800.0, 99.1, 0, 1);
        n[i++] = new Oromisthios("Tolis O", 450.0, 32, 4);
        n[i++] = new Symvasiouho("Kostas S", 1000.0, 200.0, 6, 2);

        System.out.println("====================================================================");

        for (i = 0; i < n.length; i++) {

            System.out.println("\n");

            if (n[i] instanceof Misthotos) {
                try {
                    n[i].Katharos_Misthotou();
                } catch (BonusException ex) {
                    System.out.println(ex);
                }
            } else if (n[i] instanceof Oromisthios) {
                n[i].Katharos_Oromisthiou();
            } else if (n[i] instanceof Symvasiouho) {
                n[i].Katharos_Symvasiouhou();
            }
            System.out.println("====================================================================");

        }

    }
}
