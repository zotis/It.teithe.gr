
abstract class Antiprosopoi_Poliseon {

    protected String eponimia;
    double posoPoliseon;
    int CityType;
    int Kodikos_Bonus;

    public Antiprosopoi_Poliseon(String eponimia, double posoPoliseon, int CityType, int Kodikos_Bonus) {
        this.eponimia = eponimia;
        this.posoPoliseon = posoPoliseon;
            this.CityType = CityType;
       
        
        if (Kodikos_Bonus == 0 || Kodikos_Bonus == 1) {
        this.Kodikos_Bonus = Kodikos_Bonus;
        } else {
            System.out.println("Kodikos_Bonus must be 1 or 0");
        }
    }

    
    public String getCityType(){
        if (  CityType==1   ){
            return "Athina";
        }else if (CityType==2){
            return "Thessaloniki";
        }else if (CityType==3){
            return "Other Cities";
        }
        return null;
    }
    
    abstract public String getBonusType();
            
    abstract public void TelikoPosoPromitheias();

    @Override
    public String toString() {
        return String.format("%15s%15s%15s%15s","|Eponimia=" + eponimia ,  "| PosoPoliseon=" + posoPoliseon , "| CityType=" + CityType , "| Kodikos_Bonus=" + Kodikos_Bonus );
    }
    
    
    

}

class Antiprosopos_Athinas extends Antiprosopoi_Poliseon{

    
    double exoda_diafimisis;
    double exoda_metakinisTaxidion;

    public Antiprosopos_Athinas(String eponimia, double posoPoliseon, int Kodikos_Bonus, double exoda_diafimisis , double exoda_metakinisTaxidion) {
        super(eponimia, posoPoliseon, 1, Kodikos_Bonus);
        this.exoda_diafimisis=exoda_diafimisis;
        this.exoda_metakinisTaxidion=exoda_metakinisTaxidion;
    }

    public Antiprosopos_Athinas(String eponimia, double posoPoliseon, int CityType, int Kodikos_Bonus) {
        super(eponimia, posoPoliseon, CityType, Kodikos_Bonus);
    }

    
    
   
  

    @Override
    public String getBonusType() {
        if(Kodikos_Bonus==0){
            return "No Bonus";
            }else if (Kodikos_Bonus==1){
            return "Bonus";
            }
            return null;
    }

    @Override
    public void TelikoPosoPromitheias() {
        
        if (Kodikos_Bonus==1){
             System.out.println("TelikoPosoPromitheias = "+posoPoliseon*0.03+exoda_diafimisis+exoda_metakinisTaxidion+1000);
            }else{
         System.out.println( "TelikoPosoPromitheias = "+posoPoliseon*0.03+exoda_diafimisis+exoda_metakinisTaxidion);
        }
    }

    @Override
    public String toString() {
        return String.format("%15s%15s%15s%15s%15s%15s","|Eponimia=" + eponimia ,  "| PosoPoliseon=" + posoPoliseon , "| CityType=" + CityType , "| Kodikos_Bonus=" + Kodikos_Bonus ,"| Exoda_metakinisTaxidion=" + exoda_metakinisTaxidion ,"| Exoda_diafimisis=" + exoda_diafimisis );
    }

    
    

}

class Antiprosopos_Salonikis extends Antiprosopoi_Poliseon{
    
        double exoda_metakinisTaxidion;

        
    public Antiprosopos_Salonikis( String eponimia, double posoPoliseon, int Kodikos_Bonus) {
        super(eponimia, posoPoliseon, 2, Kodikos_Bonus);
    }

   
    
    public Antiprosopos_Salonikis( String eponimia, double posoPoliseon, int Kodikos_Bonus ,double exoda_metakinisTaxidion) {
        super(eponimia, posoPoliseon, 2, Kodikos_Bonus);
        this.exoda_metakinisTaxidion = exoda_metakinisTaxidion;
    }


    @Override
    public String getBonusType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void TelikoPosoPromitheias() {

        if (Kodikos_Bonus==1){
             System.out.println("TelikoPosoPromitheias = "+posoPoliseon*0.05+exoda_metakinisTaxidion+2000);
            }else{
         System.out.println( "TelikoPosoPromitheias = "+posoPoliseon*0.05+exoda_metakinisTaxidion);
        }    
    }

    @Override
    public String toString() {
        return String.format("%15s%15s%15s%15s%15s","|Eponimia=" + eponimia ,  "| PosoPoliseon=" + posoPoliseon , "| CityType=" + CityType , "| Kodikos_Bonus=" + Kodikos_Bonus ,"| Exoda_metakinisTaxidion=" + exoda_metakinisTaxidion );
    }
    
    
    

}
class Antiprosopos_OtherCity extends Antiprosopoi_Poliseon{

   
   
    
    public Antiprosopos_OtherCity(String eponimia, double posoPoliseon, int Kodikos_Bonus) {
        super(eponimia, posoPoliseon, 3, Kodikos_Bonus);
    }

    @Override
    public String getBonusType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void TelikoPosoPromitheias() {
       if (Kodikos_Bonus==1){
             System.out.println("TelikoPosoPromitheias = "+posoPoliseon*0.05+5000);
            }else{
         System.out.println( "TelikoPosoPromitheias = "+posoPoliseon*0.05);
        }
    }
    
    @Override
    public String toString() {
        return String.format("%15s%15s%15s%15s","|Eponimia=" + eponimia ,  "| PosoPoliseon=" + posoPoliseon , "| CityType=" + CityType , "| Kodikos_Bonus=" + Kodikos_Bonus );
    }
}

class TestAntipropi{
    public static void main(String[] args){
        int i=0;
        Antiprosopoi_Poliseon[] n = new Antiprosopoi_Poliseon[3];
        
        n[i++]= new Antiprosopos_Athinas("Test1",2000,0,100.0,2.1);
        n[i++]= new Antiprosopos_Salonikis("Test2",2000,0,100.0);
        n[i++]= new Antiprosopos_OtherCity("Test3",2000,0);

        
        for( i=0  ; i < n.length;i++){
          System.out.println(n[i].toString()); 
          n[i].TelikoPosoPromitheias();
        }
    }
    
}
